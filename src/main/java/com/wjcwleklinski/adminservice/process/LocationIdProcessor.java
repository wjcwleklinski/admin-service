package com.wjcwleklinski.adminservice.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class LocationIdProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String location = exchange.getMessage().getHeader(HttpHeaders.LOCATION, String.class);
        if (StringUtils.isNotBlank(location)) {
            String[] segments = location.split("/");
            String userId = segments[segments.length - 1];
            exchange.getMessage().setHeader("Location-ID", userId);
        }
    }
}
