package com.wjcwleklinski.adminservice.rest.error;

public record Violation(
        String property,
        String message
) {
}
