package com.wjcwleklinski.adminservice.rest.error;


public record ErrorResponse(
        String code,
        String path,
        String cause,
        String message
) {
}
