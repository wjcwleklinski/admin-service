package com.wjcwleklinski.adminservice.rest.error;

import java.util.List;

public record ErrorViolationsResponse(
    String code,
    String path,
    List<Violation> violations
) {
}
