package com.wjcwleklinski.adminservice.rest.model;

import java.time.LocalDate;

public record AttributesRepresentation(
        LocalDate[] birthDate
) {
}
