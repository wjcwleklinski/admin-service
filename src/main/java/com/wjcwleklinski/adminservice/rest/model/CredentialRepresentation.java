package com.wjcwleklinski.adminservice.rest.model;

public record CredentialRepresentation(
        String type,
        String value
) {
}
