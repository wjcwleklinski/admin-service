package com.wjcwleklinski.adminservice.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public record UserRepresentation (
        @JsonIgnore
        String id,
        String username,
        String firstName,
        String lastName,
        @NotBlank
        String email,
        boolean emailVerified,
        boolean enabled,
        AttributesRepresentation attributes,
        @NotEmpty
        List<CredentialRepresentation> credentials
) {
}
