package com.wjcwleklinski.adminservice.rest.route;

import com.wjcwleklinski.adminservice.AdminAuthProperties;
import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Stream;


@Component
@RequiredArgsConstructor
public class AdminCliAuthRoute extends RouteBuilder {

    private final AdminAuthProperties properties;
    private final AccessTokenService accessTokenService;

    @Override
    public void configure() throws Exception {
        from("timer://scheduler?period=" + properties.refreshPeriodMs())
                .id(Routes.AUTHORIZATION.getId())
                .to(Routes.AUTHORIZATION.directRoute());

        from(Routes.AUTHORIZATION.directRoute())
                .id(Routes.AUTHORIZATION.clientId())
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.POST.name()))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .setBody(constant(getCredentials()))
                .to(properties.authUrl())
                .unmarshal().json(JsonLibrary.Jackson)
                .process(exchange -> {
                    var responseBody = exchange.getIn().getBody(Map.class);
                    var accessToken = responseBody.get("access_token").toString();
                    accessTokenService.setAccessToken(accessToken);
                })
                .log("Periodic authentication performed");
    }

    private String getCredentials() {
        return String.format("client_id=%s&username=%s&password=%s&grant_type=password",
                Stream.of(properties.clientId(), properties.username(), properties.password())
                        .map(prop -> URLEncoder.encode(prop, StandardCharsets.UTF_8))
                        .toArray()
        );
    }
}
