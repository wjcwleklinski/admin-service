package com.wjcwleklinski.adminservice.rest.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjcwleklinski.adminservice.rest.RestError;
import com.wjcwleklinski.adminservice.rest.error.ErrorResponse;
import com.wjcwleklinski.adminservice.rest.error.ErrorViolationsResponse;
import com.wjcwleklinski.adminservice.rest.error.Violation;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.http.base.HttpOperationFailedException;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ExceptionHandlingRouteBuilder extends RouteBuilder {

    protected final ObjectMapper objectMapper;

    @Override
    public void configure() throws Exception {
        onException(Exception.class)
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .process(exchange -> {
                    Throwable exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
                    String path = exchange.getIn().getHeader("CamelServletContextPath", String.class);
                    if (exception != null) {
                        log.error("Error occurred", exception);
                        if (exception instanceof BeanValidationException) {
                            ErrorViolationsResponse errorViolationsResponse = new ErrorViolationsResponse(
                                    HttpStatus.BAD_REQUEST.name(),
                                    path,
                                    getViolations((BeanValidationException) exception)
                            );
                            exchange.getIn().setBody(errorViolationsResponse);
                            exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.BAD_REQUEST.value());
                        } else if (exception instanceof HttpOperationFailedException) {
                            HttpOperationFailedException httpException = (HttpOperationFailedException) exception;
                            ErrorResponse errorResponse = new ErrorResponse(
                                    httpException.getHttpResponseStatus().toUpperCase(),
                                    path,
                                    "HTTP_OPERATION_FAILED",
                                    "Http operation failed"
                            );
                            exchange.getIn().setBody(errorResponse);
                            exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, httpException.getHttpResponseCode());
                        } else {
                            RestError error = RestError.getError(exception);
                            ErrorResponse errorResponse = new ErrorResponse(
                                    error.getResponseStatus().name(),
                                    path,
                                    error.name(),
                                    error.getMessage(exception)
                            );
                            exchange.getIn().setBody(errorResponse);
                            exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, error.getResponseStatus().value());
                        }
                    }
                })
                .marshal(new JacksonDataFormat(objectMapper, ErrorResponse.class))
                .handled(true);
    }

    private List<Violation> getViolations(BeanValidationException exception) {
        return exception.getConstraintViolations().stream()
                .map(constraintViolation -> new Violation(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()))
                .collect(Collectors.toList());
    }
}
