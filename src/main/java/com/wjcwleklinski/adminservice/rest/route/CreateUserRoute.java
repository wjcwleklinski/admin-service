package com.wjcwleklinski.adminservice.rest.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjcwleklinski.adminservice.process.LocationIdProcessor;
import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.rest.model.UserRepresentation;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import com.wjcwleklinski.adminservice.service.UriService;
import org.apache.camel.Exchange;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class CreateUserRoute extends ExceptionHandlingRouteBuilder {

    private final AccessTokenService accessTokenService;
    private final UriService uriService;
    private final LocationIdProcessor locationIdProcessor;

    public CreateUserRoute(ObjectMapper objectMapper,
                           AccessTokenService accessTokenService,
                           UriService uriService,
                           LocationIdProcessor locationIdProcessor) {
        super(objectMapper);
        this.accessTokenService = accessTokenService;
        this.uriService = uriService;
        this.locationIdProcessor = locationIdProcessor;
    }

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("admin/users")
                .id(Routes.CREATE_USER.getId())
                .post()
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .to(Routes.CREATE_USER.directRoute());

        from(Routes.CREATE_USER.directRoute())
                .id(Routes.CREATE_USER.clientId())
                .unmarshal(new JacksonDataFormat(objectMapper, UserRepresentation.class))
                .to("bean-validator://x")
                .marshal(new JacksonDataFormat(objectMapper, UserRepresentation.class))
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.POST.name()))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON_VALUE))
                .process(accessTokenService.addAccessTokenHeader())
                .toD(uriService.getUriRoute("/users"))
                .process(locationIdProcessor)
                .removeHeaders("*", "Location-ID");
    }
}
