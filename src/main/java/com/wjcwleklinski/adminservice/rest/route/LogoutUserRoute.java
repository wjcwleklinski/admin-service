package com.wjcwleklinski.adminservice.rest.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import com.wjcwleklinski.adminservice.service.UriService;
import org.apache.camel.Exchange;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class LogoutUserRoute extends ExceptionHandlingRouteBuilder {

    private final AccessTokenService accessTokenService;
    private final UriService uriService;

    public LogoutUserRoute(ObjectMapper objectMapper, AccessTokenService accessTokenService, UriService uriService) {
        super(objectMapper);
        this.accessTokenService = accessTokenService;
        this.uriService = uriService;
    }

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("admin/users/{userId}/logout")
                .id(Routes.LOGOUT_USER.getId())
                .post()
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .to(Routes.LOGOUT_USER.directRoute());

        from(Routes.LOGOUT_USER.directRoute())
                .id(Routes.LOGOUT_USER.clientId())
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.POST.name()))
                .process(accessTokenService.addAccessTokenHeader())
                .toD(uriService.getUriRoute("/users/${header.userId}/logout"))
                .removeHeaders("*");
    }
}
