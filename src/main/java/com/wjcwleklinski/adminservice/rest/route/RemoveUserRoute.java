package com.wjcwleklinski.adminservice.rest.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import com.wjcwleklinski.adminservice.service.UriService;
import org.apache.camel.Exchange;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
public class RemoveUserRoute extends ExceptionHandlingRouteBuilder {

    private final AccessTokenService accessTokenService;
    private final UriService uriService;

    public RemoveUserRoute(ObjectMapper objectMapper, AccessTokenService accessTokenService, UriService uriService) {
        super(objectMapper);
        this.accessTokenService = accessTokenService;
        this.uriService = uriService;
    }

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("admin/users/{userId}")
                .id(Routes.REMOVE_USER.getId())
                .delete()
                .to(Routes.REMOVE_USER.directRoute());

        from(Routes.REMOVE_USER.directRoute())
                .id(Routes.REMOVE_USER.clientId())
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.DELETE.name()))
                .process(accessTokenService.addAccessTokenHeader())
                .toD(uriService.getUriRoute("/users/${header.userId}"))
                .removeHeaders("*");
    }
}
