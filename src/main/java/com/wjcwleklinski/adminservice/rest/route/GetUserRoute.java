package com.wjcwleklinski.adminservice.rest.route;

import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import com.wjcwleklinski.adminservice.service.UriService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetUserRoute extends RouteBuilder {

    private final AccessTokenService accessTokenService;
    private final UriService uriService;

    @Override
    public void configure() throws Exception {
        rest("admin/users/{userId}")
                .id(Routes.GET_USER.getId())
                .get()
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .to(Routes.GET_USER.directRoute());

        from(Routes.GET_USER.directRoute())
                .id(Routes.GET_USER.clientId())
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.GET.name()))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON_VALUE))
                .setBody(constant(""))
                .process(accessTokenService.addAccessTokenHeader())
                .toD(uriService.getUriRoute("/users/${header.userId}"))
                .removeHeaders("*");
    }
}
