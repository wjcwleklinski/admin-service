package com.wjcwleklinski.adminservice.rest.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wjcwleklinski.adminservice.rest.Routes;
import com.wjcwleklinski.adminservice.rest.model.UserRepresentation;
import com.wjcwleklinski.adminservice.service.AccessTokenService;
import com.wjcwleklinski.adminservice.service.UriService;
import org.apache.camel.Exchange;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ModifyUserRoute extends ExceptionHandlingRouteBuilder {

    private final AccessTokenService accessTokenService;
    private final UriService uriService;

    public ModifyUserRoute(ObjectMapper objectMapper, AccessTokenService accessTokenService, UriService uriService) {
        super(objectMapper);
        this.accessTokenService = accessTokenService;
        this.uriService = uriService;
    }

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("admin/users/{userId}")
                .id(Routes.MODIFY_USER.getId())
                .put()
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .to(Routes.MODIFY_USER.directRoute());

        from(Routes.MODIFY_USER.directRoute())
                .id(Routes.MODIFY_USER.clientId())
                .unmarshal(new JacksonDataFormat(objectMapper, UserRepresentation.class))
                .to("bean-validator://x")
                .marshal(new JacksonDataFormat(objectMapper, UserRepresentation.class))
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.PUT.name()))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON_VALUE))
                .process(accessTokenService.addAccessTokenHeader())
                .toD(uriService.getUriRoute("/users/${header.userId}"))
                .removeHeaders("*");
    }
}
