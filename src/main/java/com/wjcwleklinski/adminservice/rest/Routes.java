package com.wjcwleklinski.adminservice.rest;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Routes {

    AUTHORIZATION("authorization"),
    GET_USER("get-user"),
    CREATE_USER("post-user"),
    MODIFY_USER("put-user"),
    REMOVE_USER("delete-user"),
    LOGOUT_USER("logout-user");

    private final String id;

    public String directRoute() {
        return "direct:".concat(id);
    }

    public String clientId() {
        return "client-".concat(id);
    }

}
