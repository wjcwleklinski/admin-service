package com.wjcwleklinski.adminservice.rest;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.function.Function;

import static org.springframework.http.HttpStatus.*;

@RequiredArgsConstructor
public enum RestError {

    UNRECOGNIZED_PROPERTY(UnrecognizedPropertyException.class, "Unrecognized property: %s", BAD_REQUEST, (UnrecognizedPropertyException upe) -> new String[] {upe.getPropertyName()}),
    SERVER_ERROR(Throwable.class, "Something went wrong", INTERNAL_SERVER_ERROR);

    private final Class<? extends Throwable> cause;
    private final String message;

    @Getter
    private final HttpStatus responseStatus;

    private final Function<? extends Throwable, String[]> messageFunction;

    RestError(Class<? extends Throwable> cause, String message, HttpStatus responseStatus) {
        this.cause = cause;
        this.message = message;
        this.responseStatus = responseStatus;
        this.messageFunction = (exception) -> new String[] {};
    }

    private String getMessageString(Object... args) {
        return String.format(message, args);
    }

    /** @noinspection unchecked*/
    public <T extends Throwable> String getMessage(T exception) {
        Object[] messageArgs = ((Function<T, String[]>)messageFunction).apply(exception);
        return getMessageString(messageArgs);
    }

    public static RestError getError(Throwable error) {
        return Arrays.stream(values())
                .filter(value -> value.cause.equals(error.getClass()))
                .findFirst()
                .orElse(SERVER_ERROR);
    }
}
