package com.wjcwleklinski.adminservice;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "admin.auth")
public record AdminAuthProperties(
        Long refreshPeriodMs,
        String authUrl,
        String clientId,
        String username,
        String password
) {
}
