package com.wjcwleklinski.adminservice.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.camel.Processor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class AccessTokenService {

    @Getter
    @Setter
    private volatile String accessToken;

    public Processor addAccessTokenHeader() {
        return exchange -> exchange.getIn().setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
    }
}
