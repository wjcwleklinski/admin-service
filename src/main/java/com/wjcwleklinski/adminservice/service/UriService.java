package com.wjcwleklinski.adminservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class UriService {

    @Value("${admin.baseUrl}")
    private String baseUrl;

    public String getUriRoute(String path, Object... params) {
        return UriComponentsBuilder.fromHttpUrl(baseUrl.concat(path))
                .queryParam("bridgeEndpoint", "true")
                .build(false)
                .toUriString();
    }
}
