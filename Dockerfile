FROM eclipse-temurin:21-alpine
ENV APP_HOME=/usr/local/admin-service-app
ENV JAR_FILE=build/libs/admin-service-0.1.0-SNAPSHOT.jar
ENV REFRESH_PERIOD_MS=50000 \
    AUTH_URL=https://localhost:8080/realms/master/protocol/openid-connect/token \
    CLIENT_ID="admin-cli" \
    USERNAME="admin" \
    PASSWORD="admin" \
    CHECKLISTS_REALM_BASE_URL="https://localhost/admin/realms/checklist-realm" \
    JAVA_TEMPDIR=${APP_HOME}/tmp \
    JAVA_ARGS=""
COPY ${JAR_FILE} ${APP_HOME}/app.jar

USER root
RUN chown -R 1001:0 ${APP_HOME} && mkdir ${JAVA_TEMPDIR} && chmod -R ug+rwX ${APP_HOME}
USER 1001

CMD cd ${APP_HOME} && java -Djava.io.tmpdir=${JAVA_TEMPDIR} ${JAVA_ARGS} -jar -Dadmin.auth.refreshPeriodMs=${REFRESH_PERIOD_MS} -Dadmin.auth.authUrl=${AUTH_URL} -Dadmin.auth.clientId=${CLIENT_ID} -Dadmin.auth.username=${USERNAME} -Dadmin.auth.password=${PASSWORD} -Dadmin.baseUrl=${CHECKLISTS_REALM_BASE_URL} app.jar
